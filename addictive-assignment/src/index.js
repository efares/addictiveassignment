/*
 * This is was written by Elias Fares for the purpose of completing
 * the UI/UX Developer Assignment by Addictive Mobility.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import Button from 'react-md/lib/Buttons/Button';
import TextField from 'react-md/lib/TextFields';
import Dialog from 'react-md/lib/Dialogs';
import List from 'react-md/lib/Lists/List';
import ListItem from 'react-md/lib/Lists/ListItem';
import DatePicker from 'react-md/lib/Pickers/DatePickerContainer';
import FontIcon from 'react-md/lib/FontIcons';
import './index.css';

// Remind Me Later Widget
class RemindMeLaterWidget extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: null,
      visible: false
    };
    this.openDatePicker = this.openDatePicker.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleVisibilityChange = this.handleVisibilityChange.bind(this);
  }

  openDatePicker() {
    this.setState({ visible: true });
  }

  handleChange(value) {
    this.setState({ value });
  }

  handleVisibilityChange(visible) {
    this.setState({ visible });
  }

  render() {
    const { value, visible } = this.state;
    return (
      <div>
        <Button
          flat
          secondary
          label="Remind Me Later"
          className="full-width"
          iconClassName="fa fa-bell-o"
          onClick={this.openDatePicker}
        />
        <div className="md-grid thisDatePicker">
          <DatePicker
            id="datePickerComponent"
            label="Select some date"
            displayMode="portrait"
            className="md-cell"
            visible={visible}
            value={value}
            onChange={this.handleChange}
            onVisibilityChange={this.handleVisibilityChange}
          />
        </div>
      </div>
    );
  }
}

// Share Widget
class ShareWidget extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visible: false
    };
  }

  openShareDialog = () => {
    this.setState({ visible: true });
  };

  closeShareDialog = () => {
    this.setState({ visible: false });
  };

  render() {
    const { visible } = this.state;

    const text = [
      'Sharing is caring :)',
    ].map((primaryText, i) => (
      <ListItem key={i} onClick={this.closeShareDialog} primaryText={primaryText} />
    ));

    return (
      <div>
        <Button
          raised
          secondary
          label="Share"
          className="full-width"
          iconClassName="fa fa-share-alt"
          onClick={this.openShareDialog}
        />
        <Dialog
          id="simpleDialogExample"
          visible={visible}
          title="SHARE"
          onHide={this.closeShareDialog}
        >
          <List>
            {text}
          </List>
        </Dialog>
      </div>
    );
  }
}

// Progress Bar Widget
class ProgressBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pledged: 157000,
      goal: 200000
    };
  }

  render() {
    // This could have been done in one line, but decided not to for readability purposes
    let progressPercentage = (this.state.pledged / this.state.goal) * 100;
    let progressBarStyle = {width: progressPercentage + '%'};

    // I would see the length of the number to determine where to put the comma but for the sake of this assignment,
    // I'm just going to put a comma after the 3rd digit.
    let pledgedString = [this.state.pledged.toString().slice(0, 3), ',', this.state.pledged.toString().slice(3)].join('');
    let goalString = [this.state.goal.toString().slice(0, 3), ',',this.state.goal.toString().slice(3)].join('');

    return (
      <div className="tooltip">
        <div className="progress">
          <div className="progress-bar" style={progressBarStyle}></div>
        </div>
        <span className="tooltiptext">${pledgedString} pledged of ${goalString} goal</span>
      </div>
    );
  }
}

class ContentInfo extends React.Component {
  render() {
    return (
      <div>
        <p className="text-info">Only 3 days left to fund this project.</p>
        <p className="text-info">Join the 4,221 other donors who have already supported this project. <br />Every dollar helps.</p>
      </div>
    );
  }
}

class DonateWidget extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
      submittedValue: '',
      error: false,
      visible: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  // This is called when user submits their donation amount. We should clear the inputValue when that happens
  submitDonation = () => {
    // Check if user entered a value greater than 0 or if they entered anything at all
    if (this.state.inputValue > 0 && this.state.inputValue.length > 0) {
      this.setState({
        submittedValue: this.state.inputValue,
        inputValue: '',
        error: false,
        visible: true,
       });
      // I could also increment the pledged amount by the submittedValue
    } else {
      this.setState({
        error: true
      });
    }
  };

  closeDonateDialog = () => {
    this.setState({ visible: false });
  };

  handleChange(event) {
    this.setState({
      inputValue: event
    });
  }

  render() {
    const text = [
      'Thank you for donating $' + this.state.submittedValue + '.00',
    ].map((primaryText, i) => (
      <ListItem key={i} onClick={this.closeDonateDialog} primaryText={primaryText} />
    ));

    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <div className="md-grid">
            <TextField
              placeholder="50"
              size={10}
              min={0}
              pattern="^\d+(\.|\,)\d{2}"
              type="number"
              customSize="title"
              lineDirection="left"
              className="md-cell--6 md-cell--middle donation-input"
              leftIcon={<FontIcon>attach_money</FontIcon>}
              error={this.state.error}
              errorText="Please enter a number greater than 0"
              helpOnFocus
              helpText="You are awesome!"
              value={this.state.inputValue}
              onChange={this.handleChange}
            />
            <Button
              type="submit"
              raised
              primary
              label="Donate Now"
              iconClassName="fa fa-check"
              className="md-cell--6 md-cell--middle"
              onClick={this.submitDonation}
            />
          </div>
        </form>
        <p className="text-info">Most people donate $50.</p>

        <Dialog
          id="simpleDialogExample"
          visible={this.state.visible}
          title="WOOHOO!"
          onHide={this.closeDonateDialog}
        >
          <List>
            {text}
          </List>
        </Dialog>
      </div>
    );
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({
      inputValue: this.state.inputValue
    });
  }
}

// Main Class
class App extends React.Component {
  render() {
    return (
      <div className="container">
        <div className="content">

          {/* Progress Bar  */}
          <div className="progress-bar-widget">
            <ProgressBar />
          </div>

          {/* Content Information  */}
          <div className="content-info">
            <ContentInfo />
          </div>

          {/* Donate Widget  */}
          <div className="donate-widget">
            <DonateWidget />
          </div>

        </div>
        {/* End of content div */}

        <div className="md-grid">
          <div className="md-cell--6">
            <RemindMeLaterWidget />
          </div>
          <div className="md-cell--6">
            <ShareWidget />
          </div>
        </div>

      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <App />, document.getElementById('root')
);
